$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("classpath:features/functional-acceptance-test.feature");
formatter.feature({
  "name": "Process IE001 message on actual environment",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Create h7 declaration successfully",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@IDMS-FAAT"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "prepare message from template \u0027IE001_ReviewValidTags\u0027",
  "keyword": "Given "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.prepare_message_from_template(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Submit IE001 message to the environment",
  "keyword": "When "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.submit_IE001_message()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "status code is \"Accepted\"",
  "keyword": "Then "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.status_code_is(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a response message of type \u0027IE928\u0027 is received",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.a_response_message_of_type_is_received(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "element \u0027CorrelationId\u0027 matches pattern \u0027^[0-9a-f]{8}-[0-9a-f]{4}-[4][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.element_a_matches_pattern(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "save \u0027CorrelationId\u0027 for later reference as \u0027CorrelationId\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.save_for_later_reference_as(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "in eo notification message received of type \u0027IE428\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.fetch_notification_of_type_from_eonw(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Verify IDMS system processing response from IOSS successfully to register declaration.",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@IDMS-FAAT"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "prepare message from template \u0027IE001_ReviewValidTags\u0027",
  "keyword": "Given "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.prepare_message_from_template(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "add the following fields in the message template",
  "rows": [
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {}
  ],
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.add_the_following_fields_in_the_message_template(io.cucumber.datatable.DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "update the following fields in the message template",
  "rows": [
    {},
    {},
    {},
    {},
    {},
    {}
  ],
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.update_the_following_fields_in_the_message_template(io.cucumber.datatable.DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "remove the following fields in the message template",
  "rows": [
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {}
  ],
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.remove_the_following_fields_in_the_message_template(io.cucumber.datatable.DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "remove the following fields in the message template",
  "rows": [
    {},
    {},
    {},
    {},
    {},
    {}
  ],
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.remove_the_following_fields_in_the_message_template(io.cucumber.datatable.DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Submit IE001 message to the environment",
  "keyword": "When "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.submit_IE001_message()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "status code is \"Accepted\"",
  "keyword": "Then "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.status_code_is(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a response message of type \u0027IE928\u0027 is received",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.a_response_message_of_type_is_received(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "element \u0027CorrelationId\u0027 matches pattern \u0027^[0-9a-f]{8}-[0-9a-f]{4}-[4][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.element_a_matches_pattern(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "save \u0027CorrelationId\u0027 for later reference as \u0027CorrelationId\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.save_for_later_reference_as(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "in eo notification message received of type \u0027IE428\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.fetch_notification_of_type_from_eonw(java.lang.String)"
});
formatter.result({
  "error_message": "java.lang.RuntimeException: No message of type IE428v1 was received on EOGW API in 40 seconds. Following message types were received \u0027\u0027IE416v1\u0027\n\tat be.fgov.minfin.idms.cucumber.BaseStepDefinitions.waitForMessageOfType(BaseStepDefinitions.java:92)\n\tat be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.fetch_notification_of_type_from_eonw(DeclarationStepDefinitions.java:361)\n\tat ✽.in eo notification message received of type \u0027IE428\u0027(classpath:features/functional-acceptance-test.feature:71)\n",
  "status": "failed"
});
formatter.step({
  "name": "in eo notification db message received of type \u0027IE428\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.in_eo_notification_db_message_received_of_type(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Verify IDMS system processing response from VAT successfully to register declaration.",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@IDMS-FAAT"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "prepare message from template \u0027IE001_ReviewValidTags\u0027",
  "keyword": "Given "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.prepare_message_from_template(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "update the following fields in the message template",
  "rows": [
    {},
    {},
    {},
    {},
    {}
  ],
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.update_the_following_fields_in_the_message_template(io.cucumber.datatable.DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "remove the following fields in the message template",
  "rows": [
    {},
    {},
    {},
    {},
    {},
    {},
    {}
  ],
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.remove_the_following_fields_in_the_message_template(io.cucumber.datatable.DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "remove the following fields in the message template",
  "rows": [
    {},
    {},
    {},
    {},
    {},
    {}
  ],
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.remove_the_following_fields_in_the_message_template(io.cucumber.datatable.DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Submit IE001 message to the environment",
  "keyword": "When "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.submit_IE001_message()"
});
formatter.result({
  "error_message": "java.lang.RuntimeException: Could not parse message as json\n\tat be.fgov.minfin.idms.cucumber.BaseStepDefinitions.parseJSON(BaseStepDefinitions.java:128)\n\tat be.fgov.minfin.idms.cucumber.BaseStepDefinitions.generateNewToken(BaseStepDefinitions.java:341)\n\tat be.fgov.minfin.idms.cucumber.BaseStepDefinitions.submitPostRequestWithToken(BaseStepDefinitions.java:329)\n\tat be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.submit_IE001_message(DeclarationStepDefinitions.java:1150)\n\tat ✽.Submit IE001 message to the environment(classpath:features/functional-acceptance-test.feature:102)\nCaused by: Unexpected character (\u003c) at position 0.\n\tat org.json.simple.parser.Yylex.yylex(Yylex.java:610)\n\tat org.json.simple.parser.JSONParser.nextToken(JSONParser.java:269)\n\tat org.json.simple.parser.JSONParser.parse(JSONParser.java:118)\n\tat org.json.simple.parser.JSONParser.parse(JSONParser.java:81)\n\tat org.json.simple.parser.JSONParser.parse(JSONParser.java:75)\n\tat be.fgov.minfin.idms.cucumber.BaseStepDefinitions.parseJSON(BaseStepDefinitions.java:126)\n\tat be.fgov.minfin.idms.cucumber.BaseStepDefinitions.generateNewToken(BaseStepDefinitions.java:341)\n\tat be.fgov.minfin.idms.cucumber.BaseStepDefinitions.submitPostRequestWithToken(BaseStepDefinitions.java:329)\n\tat be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.submit_IE001_message(DeclarationStepDefinitions.java:1150)\n\tat be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions$$FastClassBySpringCGLIB$$106b618f.invoke(\u003cgenerated\u003e)\n\tat org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218)\n\tat org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:771)\n\tat org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\n\tat org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:749)\n\tat org.springframework.transaction.interceptor.TransactionAspectSupport.invokeWithinTransaction(TransactionAspectSupport.java:367)\n\tat org.springframework.transaction.interceptor.TransactionInterceptor.invoke(TransactionInterceptor.java:118)\n\tat org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:186)\n\tat org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:749)\n\tat org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:691)\n\tat be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions$$EnhancerBySpringCGLIB$$c66482fc.submit_IE001_message(\u003cgenerated\u003e)\n\tat java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n\tat java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n\tat java.base/jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.base/java.lang.reflect.Method.invoke(Method.java:567)\n\tat io.cucumber.java.Invoker.invoke(Invoker.java:27)\n\tat io.cucumber.java.JavaStepDefinition.execute(JavaStepDefinition.java:27)\n\tat io.cucumber.core.runner.PickleStepDefinitionMatch.runStep(PickleStepDefinitionMatch.java:63)\n\tat io.cucumber.core.runner.TestStep.executeStep(TestStep.java:64)\n\tat io.cucumber.core.runner.TestStep.run(TestStep.java:49)\n\tat io.cucumber.core.runner.PickleStepTestStep.run(PickleStepTestStep.java:46)\n\tat io.cucumber.core.runner.TestCase.run(TestCase.java:51)\n\tat io.cucumber.core.runner.Runner.runPickle(Runner.java:66)\n\tat io.cucumber.junit.PickleRunners$NoStepDescriptions.run(PickleRunners.java:149)\n\tat io.cucumber.junit.FeatureRunner.runChild(FeatureRunner.java:83)\n\tat io.cucumber.junit.FeatureRunner.runChild(FeatureRunner.java:24)\n\tat org.junit.runners.ParentRunner$4.run(ParentRunner.java:331)\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:79)\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:329)\n\tat org.junit.runners.ParentRunner.access$100(ParentRunner.java:66)\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:293)\n\tat org.junit.runners.ParentRunner$3.evaluate(ParentRunner.java:306)\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:413)\n\tat io.cucumber.junit.Cucumber.runChild(Cucumber.java:185)\n\tat io.cucumber.junit.Cucumber.runChild(Cucumber.java:83)\n\tat org.junit.runners.ParentRunner$4.run(ParentRunner.java:331)\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:79)\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:329)\n\tat org.junit.runners.ParentRunner.access$100(ParentRunner.java:66)\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:293)\n\tat io.cucumber.junit.Cucumber$RunCucumber.evaluate(Cucumber.java:219)\n\tat org.junit.internal.runners.statements.RunBefores.evaluate(RunBefores.java:26)\n\tat org.junit.internal.runners.statements.RunAfters.evaluate(RunAfters.java:27)\n\tat org.junit.runners.ParentRunner$3.evaluate(ParentRunner.java:306)\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:413)\n\tat org.junit.runner.JUnitCore.run(JUnitCore.java:137)\n\tat org.junit.runner.JUnitCore.run(JUnitCore.java:115)\n\tat org.junit.vintage.engine.execution.RunnerExecutor.execute(RunnerExecutor.java:43)\n\tat java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)\n\tat java.base/java.util.stream.ReferencePipeline$3$1.accept(ReferencePipeline.java:195)\n\tat java.base/java.util.Iterator.forEachRemaining(Iterator.java:133)\n\tat java.base/java.util.Spliterators$IteratorSpliterator.forEachRemaining(Spliterators.java:1801)\n\tat java.base/java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:484)\n\tat java.base/java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:474)\n\tat java.base/java.util.stream.ForEachOps$ForEachOp.evaluateSequential(ForEachOps.java:150)\n\tat java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.evaluateSequential(ForEachOps.java:173)\n\tat java.base/java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)\n\tat java.base/java.util.stream.ReferencePipeline.forEach(ReferencePipeline.java:497)\n\tat org.junit.vintage.engine.VintageTestEngine.executeAllChildren(VintageTestEngine.java:82)\n\tat org.junit.vintage.engine.VintageTestEngine.execute(VintageTestEngine.java:73)\n\tat org.junit.platform.launcher.core.DefaultLauncher.execute(DefaultLauncher.java:220)\n\tat org.junit.platform.launcher.core.DefaultLauncher.lambda$execute$6(DefaultLauncher.java:188)\n\tat org.junit.platform.launcher.core.DefaultLauncher.withInterceptedStreams(DefaultLauncher.java:202)\n\tat org.junit.platform.launcher.core.DefaultLauncher.execute(DefaultLauncher.java:181)\n\tat org.junit.platform.launcher.core.DefaultLauncher.execute(DefaultLauncher.java:128)\n\tat org.apache.maven.surefire.junitplatform.JUnitPlatformProvider.invokeAllTests(JUnitPlatformProvider.java:150)\n\tat org.apache.maven.surefire.junitplatform.JUnitPlatformProvider.invoke(JUnitPlatformProvider.java:124)\n\tat org.apache.maven.surefire.booter.ForkedBooter.invokeProviderInSameClassLoader(ForkedBooter.java:384)\n\tat org.apache.maven.surefire.booter.ForkedBooter.runSuitesInProcess(ForkedBooter.java:345)\n\tat org.apache.maven.surefire.booter.ForkedBooter.execute(ForkedBooter.java:126)\n\tat org.apache.maven.surefire.booter.ForkedBooter.main(ForkedBooter.java:418)\n",
  "status": "failed"
});
formatter.step({
  "name": "status code is \"Accepted\"",
  "keyword": "Then "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.status_code_is(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "a response message of type \u0027IE928\u0027 is received",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.a_response_message_of_type_is_received(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "element \u0027CorrelationId\u0027 matches pattern \u0027^[0-9a-f]{8}-[0-9a-f]{4}-[4][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.element_a_matches_pattern(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "save \u0027CorrelationId\u0027 for later reference as \u0027CorrelationId\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.save_for_later_reference_as(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "in eo notification message received of type \u0027IE428\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.fetch_notification_of_type_from_eonw(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "in eo notification db message received of type \u0027IE428\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.in_eo_notification_db_message_received_of_type(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Create h6 declaration successfully",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@IDMS-FAAT"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "prepare message from template \u0027IE415_ReviewMultiple_GoodsShipmentItems\u0027",
  "keyword": "Given "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.prepare_message_from_template(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Submit IE415 message to the environment",
  "keyword": "When "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.submit_IE415_message()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "status code is \"Accepted\"",
  "keyword": "Then "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.status_code_is(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a response message of type \u0027IE928\u0027 is received",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.a_response_message_of_type_is_received(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "element \u0027CorrelationId\u0027 matches pattern \u0027^[0-9a-f]{8}-[0-9a-f]{4}-[4][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.element_a_matches_pattern(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "save \u0027CorrelationId\u0027 for later reference as \u0027CorrelationId\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.save_for_later_reference_as(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "in eo notification message received of type \u0027IE428\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.fetch_notification_of_type_from_eonw(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "in eo notification db message received of type \u0027IE428\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.in_eo_notification_db_message_received_of_type(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Verify IDMS system processing response from VIES to register declaration.",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@IDMS-FAAT"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "prepare message from template \u0027IE415_ReviewMultiple_GoodsShipmentItems\u0027",
  "keyword": "Given "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.prepare_message_from_template(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "update the following fields in the message template",
  "rows": [
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {}
  ],
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.update_the_following_fields_in_the_message_template(io.cucumber.datatable.DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Submit IE415 message to the environment",
  "keyword": "When "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.submit_IE415_message()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "status code is \"Accepted\"",
  "keyword": "Then "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.status_code_is(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a response message of type \u0027IE928\u0027 is received",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.a_response_message_of_type_is_received(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "element \u0027CorrelationId\u0027 matches pattern \u0027^[0-9a-f]{8}-[0-9a-f]{4}-[4][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.element_a_matches_pattern(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "save \u0027CorrelationId\u0027 for later reference as \u0027CorrelationId\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.save_for_later_reference_as(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "in eo notification message received of type \u0027IE428\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.fetch_notification_of_type_from_eonw(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "in eo notification db message received of type \u0027IE428\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "be.fgov.minfin.idms.cucumber.DeclarationStepDefinitions.in_eo_notification_db_message_received_of_type(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});